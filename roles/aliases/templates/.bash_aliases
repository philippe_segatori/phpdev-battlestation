alias dcp="docker-compose"

composer () {
	docker run -ti --rm -u "$(id -u):$(id -g)" -v $(pwd):/app composer/composer:1-alpine $@;
	return $?;
}
dcpe() { docker-compose exec --user=1000 $@;}
dcpu() { docker-compose up -d --user=1000 $@;}