# Ansible PHP Dev station provider

## Prerequisites
* Install Ansible
* Install Ansible galaxy dependencies:

```bash
ansible-galaxy install -r requirements.yml
```
## Install everything

Modify the [desktop.ini](desktop.ini) file depending on the way you want to install things:

*On the same host you run the command*:

```
[desktop]
127.0.0.1 ansible_connection=local
```

*On a machine in your local network*:

```ini
[desktop]
10.157.142.121 ansible_connection=ssh ansible_user=... ansible_ssh_pass=... ansible_become_pass=...
```

```sh
ansible-playbook playbook.yml -i desktop.ini -vv
```

## Install specific component

To specifiy a components to install use the `--tags`.

Example to install zoom:

```sh
ansible-playbook playbook.yml -i desktop.ini --tags=zoom -vv
```